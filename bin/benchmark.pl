use DDP;
use strict;
use warnings;
use Time::HiRes qw/time/;
use Device::BCM2835;

Device::BCM2835::init() || die "Could not init library";
Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_07, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);

while(1){

	my $f_starttime = time();

	for (0..1000){
		Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_07, 1);
		Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_07, 0);
	}

	my $f_endtime = time();
	my $f_runtime = ($f_endtime-$f_starttime)*1000;

	p $f_runtime;
}
