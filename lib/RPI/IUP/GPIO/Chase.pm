package RPI::IUP::GPIO::Chase;

use strict;
use threads;
use warnings;
use Device::BCM2835;
use Parallel::ForkManager;

sub new {

    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_08, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_10, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_12, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_16, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_18, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_22, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_24, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_26, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_26, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);

    return bless {
        worker => my $pm = Parallel::ForkManager->new(1),
    },
    shift;
}

sub run {
    my ( $self ) = @_;
    $self->{worker}->start;
      $self->chase_handler();
    $self->{worker}->finish;
}

sub chase_handler {
    my ( $self, $hr_params ) = @_;
    threads->create(sub { 
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_08, 1);
        Device::BCM2835::delay(500); # Milliseconds
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_10, 1);
        Device::BCM2835::delay(500); # Milliseconds
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_12, 1);
        Device::BCM2835::delay(500); # Milliseconds
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_16, 1);
        Device::BCM2835::delay(500); # Milliseconds
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_18, 1);
        Device::BCM2835::delay(500); # Milliseconds
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_22, 1);
        Device::BCM2835::delay(500); # Milliseconds
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_24, 1);
        Device::BCM2835::delay(500); # Milliseconds
        Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_26, 1);
        Device::BCM2835::delay(500); # Milliseconds
        
        $self->reset_pins;
        Device::BCM2835::delay(500); # Milliseconds
    });
}

sub reset_pins {
    my ( $self ) = @_;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_08, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_10, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_12, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_16, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_18, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_22, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_24, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_26, 0);
}

1;
