use strict;
use warnings;
use Device::BCM2835;

# call set_debug(1) to do a non-destructive test on non-RPi hardware
Device::BCM2835::set_debug($ARGV[1]);
Device::BCM2835::init() || die "Could not init library";

Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_08, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_10, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_12, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);

Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_16, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_18, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);

Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_22, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_24, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_26, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);

while (1)
{
    print qq~Turn it on\n~;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_08, 1);
    Device::BCM2835::delay(500); # Milliseconds

    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_10, 1);
    Device::BCM2835::delay(500); # Milliseconds

    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_12, 1);
    Device::BCM2835::delay(500); # Milliseconds

    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_16, 1);
    Device::BCM2835::delay(500); # Milliseconds

    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_18, 1);
    Device::BCM2835::delay(500); # Milliseconds

    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_22, 1);
    Device::BCM2835::delay(500); # Milliseconds

    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_24, 1);
    Device::BCM2835::delay(500); # Milliseconds

    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_26, 1);
    Device::BCM2835::delay(500); # Milliseconds
    
    print qq~Turn it off\n~;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_08, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_10, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_12, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_16, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_18, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_22, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_24, 0);
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_26, 0);
    Device::BCM2835::delay(500); # Milliseconds
}
