package RPI::IUP::GPIO::CommandCenter;

use strict;
use threads;
use warnings;
use Device::BCM2835;
use Parallel::ForkManager;

sub new {
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_32, &Device::BCM2835::BCM2835_GPIO_FSEL_INPT);
    Device::BCM2835::gpio_fsel(&Device::BCM2835::RPI_V2_GPIO_P1_36, &Device::BCM2835::BCM2835_GPIO_FSEL_OUTP);
    return bless {},shift;
}

sub run {
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_36, 1);
}

sub stop {
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_36, 0);
}

sub reset_pins {
    my ( $self ) = @_;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_36, 0);
}

sub gpio_pin_36_on {
    my ($self) = @_;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_36, 1);
}

sub gpio_pin_36_off {
    my ($self) = @_;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_36, 0);
}

1;
