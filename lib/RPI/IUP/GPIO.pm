package RPI::IUP::GPIO;

use strict;
use warnings;
use IUP ':all';
use Device::BCM2835;

sub new {
    return bless {}, shift,
}

sub _cb_gpio_pin_26_on {
    my ($self) = @_;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_26, 1);
}

sub _cb_gpio_pin_26_off {
    my ($self) = @_;
    Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_26, 0);
}

sub _cb_gpio_pin_36_on {
    my ($self) = @_;
    require RPI::IUP::GPIO::CommandCenter;
    $self->{command_center} //= RPI::IUP::GPIO::CommandCenter->new;
    $self->{command_center}->gpio_pin_36_on;
}

sub _cb_gpio_pin_36_off {
    my ($self) = @_;
    require RPI::IUP::GPIO::CommandCenter;
    $self->{command_center} //= RPI::IUP::GPIO::CommandCenter->new;
    $self->{command_center}->gpio_pin_36_off;
}

sub _cb_reset_pins {
    my ($self) = @_;
	Device::BCM2835::gpio_write(&Device::BCM2835::RPI_V2_GPIO_P1_26, 0);

}

sub _cb_chase {
    my ($self) = @_;
    require RPI::IUP::GPIO::Chase;
    $self->{worker} //= RPI::IUP::GPIO::Chase->new;
    $self->{worker}->run({ 'Value' => 1 });
}

sub _cb_command_center {
    my ($self) = @_;
    require RPI::IUP::GPIO::CommandCenter;
    $self->{command_center} //= RPI::IUP::GPIO::CommandCenter->new;
    $self->{command_center}->run({ 'Value' => 1 });
}


sub init_dialog {
    my ($self) = @_;

    Device::BCM2835::init() || die "Could not init library";

    $self->{dialog} = IUP::Dialog->new( TITLE=>"Custom Dialog Sample",
        MENU => IUP::Menu->new( child=>[
            IUP::Item->new(TITLE=>"Message",
                #ACTION=>\&my_cb 
            ),
            IUP::Item->new(TITLE=>"Quit", ACTION=>sub { \&_cb_reset_pins; IUP_CLOSE; } ),
        ]),  
        child => IUP::Vbox->new(MARGIN=>"5x5", ALIGNMENT=>"ARIGHT", GAP=>"5", child => [
            IUP::Hbox->new( child=>[ 
                IUP::Frame->new( TITLE=>"IUP::Button", 
                    child=> IUP::Vbox->new( child=>[
                        IUP::Button->new( ACTION=>\&_cb_gpio_pin_26_on, IMAGE=>"IUP_Tecgraf", TITLE=>"pin 26 on" ),
                        IUP::Button->new( ACTION=>\&_cb_gpio_pin_26_off, IMAGE=>"IUP_Tecgraf", TITLE=>"pin 26 off" ),
                        IUP::Button->new( ACTION=>\&_cb_chase, IMAGE=>"IUP_Tecgraf", TITLE=>"toggle chase" ),
                    ])
                ),
                IUP::Frame->new( TITLE=>"IUP::Button", 
                    child=> IUP::Vbox->new( child=>[
                        IUP::Button->new( ACTION=>\&_cb_gpio_pin_36_on, IMAGE=>"IUP_Tecgraf", TITLE=>"pin 36 on" ),
                        IUP::Button->new( ACTION=>\&_cb_gpio_pin_36_off, IMAGE=>"IUP_Tecgraf", TITLE=>"pin 36 off" ),
                        #IUP::Button->new( ACTION=>\&_cb_command_center, IMAGE=>"IUP_Tecgraf", TITLE=>"Command Center" ),
                    ])
                ),
           ] ) 
        ])
    );
    return $self;
}

sub run {
    my ($self) = @_;
    return unless $self->{dialog};
    $self->{dialog}->Show();
    IUP->MainLoop();
}

1;
