Building this module requires the bcm2835 library to be installed. You can get the latest version from http://www.airspayce.com/mikem/bcm2835/

    sudo apt-get install libgtk-3-dev libcairo2-dev libx11-dev libglu-dev freeglut3-dev
